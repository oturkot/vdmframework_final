## Extended class to store vdm data; standard+BeamBeam+FitResults
import ROOT as r
import pickle
import math
import json

from inputDataReaderII import *

class vdmInputData_DynBeta(vdmInputData):

# class inherits from vdmInputData
# contains additional functions necessary for Dynamic Beta simulations

    def __init__(self, scanNumber):

        self.scanNumber = scanNumber
        vdmInputData.__init__(self,scanNumber)

# --> data from BeamBeamFile
        # file fragment for current scan
        self.BBcorr={}
        self.bb={} 
        # BeamBeam correction per BX at scanpoints
        self.bbPerBX={}

# --> data from CapSigmaFile
        # file fragment for fit results
        self.fitres={}
        # data from fit results
        self.CapSigmaPerBX={}
        self.meanPerBX={}

# ---> additional data
        # bb-corrected scanpoints
        self.corrspPerBX={}
        #averaged bunch current
        self.averCurrPerBX={}
        #normalized beam separation per BX
        self.normBeamSepPerBX={}
        #normalized bb corrected beam separation per BX
        self.normBeamSepPerBX_BBcorr={}
        #emittance per BX
        self.emitPerBX={}

##### --> BeamBeam data reading
    def GetBeamBeamData(self, fileName):

        print "Attention from GetBeamBeamData: If IOError, check path to BeamBeamFile or run BeamBeam correction"
        table = {}
        with open(fileName, 'rb') as f:
            table = json.load(f)

        self.BBcorr=table
        #print table

        key = "Scan_" + str(self.scanNumber)
        #print key
        corrPerSP=self.BBcorr[key]
        #print corrPerSP[0].keys()
        #print corrPerSP[1]

        corrXPerSP = [{} for value in corrPerSP]
        corrYPerSP = [{} for value in corrPerSP]
        
        for value in corrPerSP:
            corrXPerSP[value['ScanPointNumber']-1] = value['corr_Xcoord']
            corrYPerSP[value['ScanPointNumber']-1] = value['corr_Ycoord']
        #print corrXPerSP

        corrXPerBX = {bx:[] for bx in self.collidingBunches}
        corrYPerBX = {bx:[] for bx in self.collidingBunches}
        #print self.collidingBunches
        BB_bxList=[]
        for i, bx in enumerate(self.collidingBunches):
            try:
                for j in range(self.nSP):
                    valueX = corrXPerSP[j][str(bx)]
                    corrXPerBX[bx].append(valueX)
                    valueY = corrYPerSP[j][str(bx)]
                    corrYPerBX[bx].append(valueY)
            except:
                print bx," is missing; don't fill corr per bx"
            else:
                BB_bxList.append(bx)

        self.bbPerBX={}
        if 'X' in self.scanName:
             self.bbPerBX=corrXPerBX
        if 'Y' in self.scanName:
             self.bbPerBX=corrYPerBX

        #print self.bbPerBX['1943']

        return

#### --> CapSigma data reading
    def GetFitResults(self,fileName):

        with open(fileName,'rb') as f:
           table=pickle.load(f)

        captions=table[0]
        idx1=-1
        idx2=-1
        for idx, cap in enumerate(captions):
            if cap=="CapSigma":
                idx1=idx
            if cap=="Mean":
                idx2=idx
        if (idx1==-1) | (idx2==-1):
            print "Fit results has not column CapSigma or Mean"
            exit() 

        for entry in table:
            if (entry[0]==str(self.scanNumber)) and (str(entry[2])!='sum'):
                bcid=entry[2]
                self.CapSigmaPerBX[str(bcid)]=entry[idx1]
                self.meanPerBX[str(bcid)]=entry[idx2]

        #print self.scanName
        #print self.CapSigmaPerBX['1072']

        return

### --> calculations within scan
### --> apply BeamBeam correction
    def applyBeamBeam(self):
      
        for i, bx in enumerate(self.usedCollidingBunches):
            if len(self.bbPerBX[bx])!=len(self.spPerBX[bx]):
                print "Error in applyBeamBeam, bx=", bx, " lengths are not equal, exit program"
                sys.exit(1)
            else:
                corrSPList=[a+b for a,b in zip(self.spPerBX[bx],self.bbPerBX[bx])]
                self.corrspPerBX[bx]=corrSPList

        #print self.corrspPerBX['1011']

        return

### --> calculate averaged current of bunch
    def averageCurrent(self):

        #the prescription from "W. Kozanecki, Updated implementation of the dynamic-b correction, LLCMWG meeting, 5 Aug 2013" is implemented
        #so, the average current is used as the intensity of opposite bunch

        for i, bx in enumerate(self.usedCollidingBunches):
            Length=len(self.spPerBX[bx])
            meanSPCurr=[]
            for idx in range(Length):
                cur1=self.avrgFbctB1PerBX[bx][idx]
                cur2=self.avrgFbctB2PerBX[bx][idx]
                meanSPCurr.append(0.5*(cur1+cur2))
            
            Current=0.0 
            for sp in range(Length):
                Current=Current+meanSPCurr[sp]

            Current=Current/Length
            self.averCurrPerBX[bx]=Current

        #print self.averCurrPerBX['1011']

        return

### --> normalize Beam Separation
    def normalizeBeamSeparation(self):

        for i,bx in enumerate(self.usedCollidingBunches):
            Length=len(self.spPerBX[bx])
            CapSigma=self.CapSigmaPerBX[bx]
            normSPList=[]
            normSPList_corr=[]
            for idx in range(Length):
                coord=self.spPerBX[bx][idx]
                normSPList.append(math.sqrt(2.0)*coord/CapSigma)
                coord=self.corrspPerBX[bx][idx]
                normSPList_corr.append(math.sqrt(2.0)*coord/CapSigma)
            self.normBeamSepPerBX[bx]=normSPList
            self.normBeamSepPerBX_BBcorr[bx]=normSPList_corr

        #print self.CapSigmaPerBX['1011']
        #print self.normBeamSepPerBX['1032']
        #print self.normBeamSepPerBX_BBcorr['1032']


        return

### --> Emittance estimation
    def calculateEmittance(self):
        
        # for protons only; beams of equal energy, energy in GeV
        gamma=float(self.energyB1)*1E3/938.272
        bStar=float(self.betaStar)
        for i,bx in enumerate(self.usedCollidingBunches):
            self.emitPerBX[bx]=gamma*self.CapSigmaPerBX[bx]*self.CapSigmaPerBX[bx]/2.0/bStar

        #print self.scanName
        #print gamma
        #print self.energyB1
        #print bStar
        #print self.CapSigmaPerBX['1072']
        #print self.emitPerBX['1072']
        return
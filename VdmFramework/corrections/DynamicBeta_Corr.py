import CorrectionManager
import json,sys

class DynamicBeta_Corr(CorrectionManager.CorrectionProvider):
# DynamicBeta correction application

    def doCorr(self, inData, filename):
    #def doCorr(self, inData):
    
        print "Dynamic Beta is applied with the file :", filename
        with open(filename) as f:
            db=json.load(f)

        for entry in inData:
            scanNumber=entry.scanNumber
            key="Scan_"+str(scanNumber)

            j=0
            for i in dict(db).keys():
                ist=str(i)
                if key in ist:
                    table=db[i]
                    j=1
                    break
            if j==0:
                print "No input for Dynamic Beta correction in", filename, "for scan", key, ", exit program"
                sys.exit()

            for bx in entry.usedCollidingBunches:
                Lumi=entry.lumiPerBX[bx]
                Lumie=entry.lumiErrPerBX[bx]
                CoordList=entry.spPerBX[bx]
                CurrB1=entry.avrgFbctB1PerBX[bx]
                CurrB2=entry.avrgFbctB2PerBX[bx]
            
                try:
                    CorrFactor=table[bx]
                except KeyError:
                    print "bx=", bx, " is omitted in doMakeDynamicBeta"
                    continue

                Length=len(Lumi)
                Length1=len(CorrFactor)
                if Length1==0:
                    print "bx=", bx, " is omitted in DynamicBeta_Corr"
                    continue

                if Length<Length1:
                    print "Error in DynamicBeta_Corr: exit program"
                    sys.exit()

                # There could be extra scanpoints in Rates: at scan start and at the end
                #print "bx=", bx, " ", Length1        
                minidx=CorrFactor[0][0]
                maxidx=CorrFactor[Length1-1][0]

                corrLumi=[]
                corrErr=[]
                Coord=[]
                Curr1=[]
                Curr2=[]
                k=0
                for idx in range(minidx,maxidx+1):
                    factor=CorrFactor[k][1]
                    val=Lumi[idx]*factor
                    #pair=(idx,val)
                    #print pair
                    corrLumi.append(val)
                    val=Lumie[idx]*factor
                    #pair=(idx,val)
                    corrErr.append(val)
                    Coord.append(CoordList[idx])
                    Curr1.append(CurrB1[idx])
                    Curr2.append(CurrB2[idx])
                    k=k+1
                    #if bx=='2734':
                    #    print factor

                #if bx=='2734':
                #    print "Scan=", entry.scanNumber
                #    print "Coord0=", entry.spPerBX[bx]
                #    print "Coord=", Coord

                entry.lumiPerBX[bx]=corrLumi
                entry.lumiErrPerBX[bx]=corrErr
                # as range of scanpoints could be changed, we have to refresh scanpoint and currents lists 
                entry.spPerBX[bx]=Coord
                entry.avrgFbctB1PerBX[bx]=Curr1
                entry.avrgFbctB2PerBX[bx]=Curr2


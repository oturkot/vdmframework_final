import tables as t, sys

folder = '/brildata/vdmdata18/'
f1 = '6868_1806302313_1806302343.hd5'   
f2 = '6868_1806302339_1807010009.hd5'

compr_filter = t.Filters(complevel=9, complib='blosc')
chunkshape=(100,)

fout = '_'.join(f1.split('_')[:-1]+f2.split('_')[-1:])

print fout

h51 = t.open_file(folder+f1)
h52 = t.open_file(folder+f2)
h5out = t.open_file(folder+fout,mode='w')

for table in h51.iter_nodes('/'):
	descr = table.description._v_colobjects.copy()
	outtable = h5out.create_table('/',table.name,descr,filters=compr_filter,chunkshape=chunkshape)
	table.append_where(outtable,condition ='timestampsec > 0')

	table2 = h52.get_node('/'+table.name)
	table2.append_where(outtable,condition ='timestampsec > 0')

h51.close()
h52.close()
h5out.close()


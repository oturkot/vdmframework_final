import tables as t, sys,pandas as pd

folder = '/brildata/vdmdata18/'
# fin = '7443_1811141822_1811141858.hd5'   
# split = '1811141837'

# fin = '7443_1811141837_1811141858.hd5'
# split = '1811141844'


fin = '7453_1811181543_1811181614.hd5'
split = '1811181602'


compr_filter = t.Filters(complevel=9, complib='blosc')
chunkshape=(100,)


chunks = fin.split('.')[0].split('_')
fout1 = '_'.join(chunks[0:-1]+[split])+'.hd5'
fout2 = '_'.join(chunks[0:1]+[split]+chunks[-1:])+'.hd5'


timestamp = str(int(pd.to_datetime(split,format='%y%m%d%H%M').value/1e9))


h5in = t.open_file(folder+fin)

h5out1 = t.open_file(folder+fout1,mode='w')
h5out2 = t.open_file(folder+fout2,mode='w')


for table in h5in.iter_nodes('/'):
	descr = table.description._v_colobjects.copy()
	outtable1 = h5out1.create_table('/',table.name,descr,filters=compr_filter,chunkshape=chunkshape)
	outtable2 = h5out2.create_table('/',table.name,descr,filters=compr_filter,chunkshape=chunkshape)
	table.append_where(outtable1,condition ='timestampsec < '+timestamp)
	table.append_where(outtable2,condition ='timestampsec > '+timestamp)



h5in.close()
h5out1.close()
h5out2.close()


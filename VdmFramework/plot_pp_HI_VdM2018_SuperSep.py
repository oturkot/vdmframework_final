#to run try e.g: python plot_HI_VdM_SuperSep.py HFET HI

import tables, pandas as pd, pylab as py, sys, numpy
from scipy import stats
import matplotlib.pyplot as plt
import matplotlib.dates as dates
import math
from datetime import datetime
import sys

if len(sys.argv) <= 2:
    print('1) User should specify detector name. Provide one of these as argument: PLT, HFOC, HFET, BCM1F, BCM1F_Si')
    print('2) User should specify pp or HI running period. Provide one of these as argument: pp, HI')
    sys.exit()

detector=sys.argv[1]
print('Running for detector:', str(sys.argv[1]))

running_period=sys.argv[2]
filen = []
N_of_BCIDs_to_plot_now = 3
bxraw_list = [[] for a in range(N_of_BCIDs_to_plot_now)] 
timestamp_list =  []
step_points = []
avg_per_step_per_bx = [[],[]]
avgErr_per_step_per_bx = [[],[]]

if running_period=="pp":
    file1 = '/brildata/18/6868/6868_319019_1807010037_1807010750.hd5' # file from 2018 pp VdM Fill 6868 for super separation
    stepsX_timestemp_list =  [[1530412500,1530412800],[1530427080,1530427440]] #super-separations steps of VdM in fill 6868
elif running_period=="HI":
    file1 = '/brildata/18/7443/7443_326718_1811141619_1811142213.hd5' # file from 2018 HI VdM Fill 7443 for super separation
    stepsX_timestemp_list =  [[1542223200, 1542223590],[1542231120, 1542231420]] #super-separations steps in HI VdM in fill 7443
else: 
    print "Unknown running period. Expected values are pp or HI."
    sys.exit()

filen.append(file1)



for f in filen:

    h5file = tables.open_file(f) 

    if detector=="PLT":
        hist = h5file.root.pltlumizero
    elif  detector=="HFOC":
        hist = h5file.root.hfoclumi
    elif  detector=="HFET":
        hist = h5file.root.hfetlumi
    elif  detector=="BCM1F":
        hist = h5file.root.bcm1flumi
    elif  detector=="BCM1F_Si":
        hist = h5file.root.bcm1fsilumi
    else:
        print "Unknown detectors name. No such table in hd5 file."
        sys.exit()


    for row in hist.iterrows():
        timestamp_list.append(row['timestampsec'])
        for bx in range(N_of_BCIDs_to_plot_now):
            bxraw_list[bx].append(row['bxraw'][bx])

for step in stepsX_timestemp_list:
    print "Processing super separation region number", stepsX_timestemp_list.index(step)+1
    for bx in range(N_of_BCIDs_to_plot_now):
        for t in timestamp_list:
            if stepsX_timestemp_list[stepsX_timestemp_list.index(step)][1] > t > stepsX_timestemp_list[stepsX_timestemp_list.index(step)][0]:
                step_points.append(bxraw_list[bx][timestamp_list.index(t)])
        step_array = numpy.array(step_points)
        step_mean = numpy.mean(step_array, axis=0)
        step_Err_old = stats.sem(step_array)
        if (step_mean*(1-step_mean)/(4*4096*len(step_points)))> 0 :
            step_Err_binom = math.sqrt(step_mean*(1-step_mean)/(4*4096*len(step_points)))
        else:
            step_Err_binom = 0

        print "bx = ", bx, " mean of the step " , step_mean, " +/-", step_Err_binom
        avg_per_step_per_bx[stepsX_timestemp_list.index(step)].append(step_mean)
        avgErr_per_step_per_bx[stepsX_timestemp_list.index(step)].append(step_Err_binom)
        step_points = []
        step_array = numpy.array(step_points) 

    print "Finished with one super-separation region..."                                            


plt.figure(facecolor="white")
plt.errorbar(
    range(N_of_BCIDs_to_plot_now), avg_per_step_per_bx[0], yerr=avgErr_per_step_per_bx[0], fmt='o',
    label="Super-separation 1")
plt.errorbar(
    range(N_of_BCIDs_to_plot_now), avg_per_step_per_bx[1], yerr=avgErr_per_step_per_bx[1], fmt='o',
    label="Super-separation 2")
plt.grid()
plt.xlabel('BCID', fontsize=16)
if detector == "HFET":
    plt.ylabel('HFET, Et [ per orbit per bx]', fontsize=16)
else:
    plt.ylabel(detector+', $\mu$ [<hits> per orbit per bx]', fontsize=16)
plt.legend()
plt.show()
